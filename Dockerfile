FROM nginxinc/nginx-unprivileged:1-alpine

LABEL maintainer="cameron.kerr.nz@gmail.com"

COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

USER root

COPY ./entrypoint.sh /entrypoint.sh

RUN install -o nginx -g nginx -m 0755 -p -d /vol/static && \
    install -o nginx -g nginx -m 0644 /dev/null /etc/nginx/conf.d/default.conf && \
    chmod +x /entrypoint.sh

USER nginx

ENTRYPOINT [ "/entrypoint.sh" ]
